import React, { Component } from "react";

export default class FormValidation extends Component {
  constructor(prop) {
    super(prop);
    this.state = {
      values: {
        manv: "",
        tennv: "",
        email: "",
      },
      errors: {
        manv: "",
        tennv: "",
        email: "",
      },
      formValid: false,
      maValid: false,
      tenValid: false,
      emailValid: false,
    };
  }

  handleOnChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      values: { ...this.state.values, [name]: value },
    });
  };

  handleErrors = (event) => {
    const { name, value } = event.target;
    let mess = value === "" ? name + " không được rỗng" : "";
    let { maValid, tenValid, emailValid } = this.state;
    console.log(name, value, mess);
    switch (name) {
      case "manv":
        //toán tử 3 ngôi
        maValid = mess !== "" ? false : true;
        //   if(mess !== ""){
        //     maValid = false;
        //   }else{
        //       maValid = true;
        //   }
        if (value && value.trim().length < 4) {
          mess = "vui long nhap do dai ky tu lon hon 3!";
          maValid = false;
        }
        break;
      case "tennv":
        tenValid = mess !== "" ? false : true;
        break;
      case "email":
        emailValid = mess !== "" ? false : true;
        if (value && !value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
          mess = "email khong dung dinh dang";
          emailValid = false;
        }
        break;
      default:
        break;
    }
    this.setState(
      {
        errors: { ...this.state, [name]: mess },
        maValid,
        tenValid,
        emailValid,
      },
      () => {
        console.log(this.state);
        this.formValidation();
      }
    );
  };

  formValidation = () => {
    let { maValid, tenValid, emailValid } = this.state;
    // const status = maValid && tenValid && emailValid;
    this.setState({
      formValid: maValid && tenValid && emailValid,
    });
  };

  render() {
    return (
      <div className="container d-block">
        <h3 className="title">*FomValidation</h3>
        <form>
          <div className="form-group">
            <label>Mã nhân viên</label>
            <input
              type="text"
              className="form-control"
              onChange={this.handleOnChange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
              name="manv"
            />
            {this.state.errors.manv !== "" ? (
              <div className="alert alert-danger">{this.state.errors.manv}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label>Tên nhân viên</label>
            <input
              type="text"
              className="form-control"
              onChange={this.handleOnChange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
              name="tennv"
            />
            {this.state.errors.tennv !== "" ? (
              <div className="alert alert-danger">
                {this.state.errors.tennv}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              className="form-control"
              onChange={this.handleOnChange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
              name="email"
            />
            {this.state.errors.email !== "" ? (
              <div className="alert alert-danger">
                {this.state.errors.email}
              </div>
            ) : (
              ""
            )}
          </div>
          <button
            type="submit"
            className="btn btn-success"
            disabled={!this.state.formValid}
          >
            Submit
          </button>
        </form>
      </div>
    );
  }
}
