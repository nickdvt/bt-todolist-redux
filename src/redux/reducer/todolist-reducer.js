let initialState = {
  taskList: [
    {
      id: 1,
      status: "todo",
      textTask: "Học REDUX",
    },
    {
      id: 2,
      status: "completed",
      textTask: "Học HTML",
    },
  ],
};
const todoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case "DELETE_TASK": {
      let deleteTask = [...state.taskList];
      const index = state.taskList.findIndex(
        (item) => item.id === action.payload.id
      );
      if (index !== -1) {
        deleteTask.splice(index, 1);
        state.taskList = deleteTask;
      }
      return { ...state };
    }

    case "UPDATE_TASK": {
      let updateTask = [...state.taskList];
      const index = state.taskList.findIndex(
        (item) => item.id === action.payload.id
      );
      if (action.payload.status === "completed") {
        updateTask[index].status = "todo";
      } else {
        updateTask[index].status = "completed";
      }
      state.taskList = [...updateTask];
      return { ...state };
    }

    case "ADD_TASK": {
      let addTask = [...state.taskList];
      const newTask = {
        id: Math.random(),
        status: "todo",
        textTask: action.payload,
      };
      addTask.push(newTask);
      state.taskList = [...addTask];
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default todoListReducer;
