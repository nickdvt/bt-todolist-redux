import {
  DELETE_USER,
  EDIT_USER,
  SUBMIT_USER,
  GET_KEYWORD,
} from "./../constant";

let initialState = {
  userList: [
    {
      id: 1,
      name: "Dinh Phuc Nguyen",
      username: "dpnguyen",
      email: "dpnguyen@gmail.com",
      phoneNumber: "1123123213",
      type: "VIP",
    },
    {
      id: 2,
      name: "hao",
      username: "nguyendp",
      email: "nguyendp@gmail.com",
      phoneNumber: "1123123213",
      type: "VIP",
    },
  ],
  userEdit: null,
  keyword: "",
};

const userReducer = (state = initialState, action) => {
  // console.log(action);
  switch (action.type) {
    case DELETE_USER: {
      /**
       * Xóa user
       */
      let userList = [...state.userList];
      const index = state.userList.findIndex(
        (user) => user.id === action.payload.id
      );
      if (index !== -1) {
        userList.splice(index, 1);
        state.userList = userList;
      }
      return { ...state };
    }

    case SUBMIT_USER: {
      let userList = [...state.userList];
      if (action.payload.id) {
        //UPDATE USER
        const index = state.userList.findIndex(
          (user) => user.id === action.payload.id
        );
        if (index !== -1) {
          userList[index] = action.payload;
        }
      } else {
        //ADD USER
        const userNew = { ...action.payload, id: Math.random() };
        userList = [...state.userList, userNew];
      }

      //Cập nhật lại state
      state.userList = userList;
      return { ...state };
    }

    case EDIT_USER: {
      state.userEdit = action.payload;
      return { ...state };
    }

    case GET_KEYWORD: {
      state.keyword = action.payload;
      return { ...state };
    }

    default:
      return { ...state };
  }
};

export default userReducer;
