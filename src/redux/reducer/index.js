import { combineReducers } from "redux";
// import userReducer from "./user";
import todoListReducer from "./todolist-reducer";

const rootReducer = combineReducers({
  //key: value
  // userReducer, //   userReducer: userReducer,
  todoListReducer,
});

export default rootReducer;
