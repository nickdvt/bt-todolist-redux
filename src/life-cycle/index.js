import React, { Component } from "react";

export default class LifeCycle extends Component {
  constructor(prop) {
    super(prop);
    this.state = {
      number: 0,
    };
    console.log("constructor - chạy 1 lần duy nhất");
  }

  UNSAFE_componentWillMount() {
    console.log("componenwillmmount - chạy 1 lần duy nhất");
  }

  componentDidMount() {
    // chạy sau render
    // goi API DOM
    console.log("componentDidMount - chạy 1 lần duy nhất và sau render");
  }

  UNSAFE_componentWillUpdate() {
    console.log(
      "componentWillUpdate - chạy sau khi có dữ liệu update - và chạy trước render"
    );
  }

  componentDidUpdate() {
    console.log(
      "componentDidUpdate - chạy sau khi có dữ liệu update - và chạy sau render "
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    //có cho quyền update hay không - bắt buộc phải return về giá trị true or false
    console.log("shouldComponentUpdate", nextProps, nextState);
    if (nextState && nextState.number === 2) {
      return false;
    }
    return true;
  }

  render() {
    console.log("render");
    return (
      <div>
        <h3>*Lif-Cycle</h3>
        <p>Number: {this.state.number}</p>
        <button
          className="btn btn-success"
          onClick={() => {
            this.setState({
              number: this.state.number + 1,
            });
          }}
        >
          Tăng số
        </button>
      </div>
    );
  }
}
