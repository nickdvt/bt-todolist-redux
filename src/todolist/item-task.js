import React, { Component } from "react";
import { connect } from "react-redux";

class ItemTask extends Component {
  render() {
    const { dataTask } = this.props;
    return (
      <li>
        <span className="liName">{dataTask.textTask}</span>
        <span>
          <i
            className="fas fa-trash-alt"
            onClick={() => this.props.DELETE_TASK(dataTask)}
          />
          <i
            className="far fa-check-circle"
            onClick={() => this.props.UPDATE_TASK(dataTask)}
          />
        </span>
      </li>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    DELETE_TASK: (task) => {
      const action = {
        type: "DELETE_TASK",
        payload: task,
      };
      dispatch(action);
    },

    UPDATE_TASK: (task) => {
      const action = {
        type: "UPDATE_TASK",
        payload: task,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemTask);
