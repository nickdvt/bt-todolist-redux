import React, { Component } from "react";
import ItemTask from "./item-task";
import { connect } from "react-redux";

class ListTask extends Component {
  renderTaskCompleted = () => {
    const { taskList } = this.props;
    return taskList.map((item) => {
      if (item.status === "completed") {
        return <ItemTask dataTask={item} key={item.id} />;
      }
    });
  };

  renderTaskToDo = () => {
    const { taskList } = this.props;
    return taskList.map((item) => {
      if (item.status === "todo") {
        return <ItemTask dataTask={item} key={item.id} />;
      }
    });
  };

  render() {
    return (
      <>
        <ul className="todo" id="todo">
          {this.renderTaskToDo()}
        </ul>
        {/* Completed tasks */}
        <ul className="todo" id="completed">
          {this.renderTaskCompleted()}
        </ul>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    taskList: state.todoListReducer.taskList,
  };
};

export default connect(mapStateToProps, null)(ListTask);
