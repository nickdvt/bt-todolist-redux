import "./App.css";
// import Baitap1 from "./baitap1";
// import Baitap2 from "./baitap2";
import RenderingElements from "./rendering-elements";
import HandlingEvent from "./handling-event";
import ExampleHandlingEvent from "./handling-event/example";
import State from "./state";
import ExampleCar from "./example-car";
import ListKeys from "./list-keys";
import ExampleList from "./list-keys/example";
import Props from "./props";
import ShoppingCart from "./shopping-cart";
// import Home from "./usermanagement";
import HomeRedux from "./usermanagement-redux";
import Todolist from "./todolist";
import FormValidation from "./form-validation";
import LifeCycle from "./life-cycle";

function App() {
  return (
    <div>
      {/* <Baitap1 /> */}
      {/* <Baitap2 /> */}
      {/* <RenderingElements /> */}
      {/* <hr /> */}
      {/* <HandlingEvent />
      <hr />
      <ExampleHandlingEvent />
      <hr />
      <State />
      <hr />
      <ExampleCar />
      <hr />
      <ListKeys />
      <hr />
      <ExampleList />
      <hr />
      <Props />
      <hr />
      <ShoppingCart />
      <hr /> */}
      {/* <Home /> */}
      {/* <HomeRedux /> */}
      {/* <hr /> */}
      <Todolist />
      {/* <hr /> */}
      {/* <FormValidation /> */}
      {/* <LifeCycle /> */}

      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}

export default App;
